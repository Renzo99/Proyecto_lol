var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const router= express.Router();

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

//conectar a mongo db con mongoose
const mongoose = require('mongoose');
var db = mongoose.connect('mongodb://tryndamere:trynda@cluster1-shard-00-00-zvcnt.mongodb.net:27017,cluster1-shard-00-01-zvcnt.mongodb.net:27017,cluster1-shard-00-02-zvcnt.' +
    'mongodb.net:27017/leagueoflegends?ssl=true&replicaSet=Cluster1-shard-0&authSource=admin&retryWrites=true', {useNewUrlParser: true})
    .then(db => console.log('Base conectada'))
    .catch(err => console.log(err));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//esquemas
const Schema = mongoose.Schema;

const Campeones = new Schema({
    firstNAme: String,
    lastName: String
});

var Campeon = mongoose.model('Campeones',Campeones);
module.exports = mongoose.model('Campeones', Campeones);

var campeon1= new Campeon({
    firstNAme: 'David',
    lastName: 'el oso'
});


/*
campeon1.save(function (err) {
   if(err) throw err;
   console.log('Campeon agregado')
});
*/
app.get('/api/characters', function(req, res){
    db.select().from("characters")
        .then(function (data) {
            res.json({champs: data});
        }).catch(function (error) {
        console.log(error);
    })
});




app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});



module.exports = app;
